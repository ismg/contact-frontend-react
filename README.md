# Software Requirement
NodeJS 14.21.3
NPM 6.14.18
# Installation of contact frontend app

1. Please complete above software installation.

2. clone project from gitlab link
[https://gitlab.com/ismg/contact-frontend-react.git](https://gitlab.com/ismg/contact-frontend-react.git)
### `git clone https://gitlab.com/ismg/contact-frontend-react.git`

3. Go to project folder. open the cmd.

4. Type following cmd for node package installation
### `npm install`

5. Then, Compile the code.
### `npm start`

5. Open Browser. open this link:
### 'localhost:3000'
import axios from 'axios';

const API_URL = '/api/'


const login = async (userData) => {
    const response = await axios.post(API_URL + 'login', userData)

    if(response.data.user)
    {
        localStorage.setItem('user', JSON.stringify({ ...response.data.user, token: response.data.access_token }))
    }

    return response.data
}

const logout = async () => {
    localStorage.setItem('user', null)
}

const authService = {
    login,
    logout
} 

export default authService
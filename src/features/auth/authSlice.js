import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";

import authService from "./authService";

//Get user from localStorage
const user = localStorage.getItem('user') == null ? null : JSON.parse(localStorage.getItem('user'))

const initialState = {
    user: user ? user : null,
    isError: false,
    isSuccess: false,
    isLoading: false,
    message: ''
}

//Login user
export const login = createAsyncThunk('auth/login',async(user, thunkAPI) => {
    try 
    {
        return await authService.login(user)
    } 
    catch (error) 
    {
        return thunkAPI.rejectWithValue(error.response.data)
    }
});

export const logout = createAsyncThunk('auth/logout', async() => {
    return await authService.logout()
})

export const authSlice = createSlice({
    name: 'auth',
    initialState,
    reducers:{
        reset: (state) => {
            state.isLoading = false
            state.isError = false
            state.isSuccess = false
            state.message = ''
        }
    },
    extraReducers: (builder) => {
        builder
            .addCase(login.pending, (state) => {
                state.isLoading = true
            })
            .addCase(login.fulfilled, (state, action) => {
                state.isLoading = false
                state.isSuccess = true
                state.user = { ...action.payload.user, token: action.payload.access_token } 
                state.message = action.payload.message
            })
            .addCase(login.rejected, (state, action) => {
                state.isLoading = false
                state.isError = true
                state.message = action.payload.message
                state.user = null
            })
            .addCase(logout.fulfilled, (state) => {
                state.user = null
            })
    }
})

export const { reset } = authSlice.actions

export default authSlice.reducer
import React from 'react'
import { useSelector } from 'react-redux'
import DataTable from "react-data-table-component";

import { debounce } from "./../app/common";
import FilterComponent from "./FilterComponent";

const ContactList = ({ contactList, totalRows, perPage, loading, fetchContacts }) => {
	const user = useSelector(state => state.auth.user)

	
    const [filterText, setFilterText] = React.useState('');	
	const [resetPaginationToggle, setResetPaginationToggle] = React.useState(false);

    const columns = [
        {
            name: 'ID',
            selector: (row) => row.id,
        },
        {
            name: 'Name',
            selector: (row) => row.name,
        },
        {
            name: 'Title',
            selector: (row) => (row?.job_titles?.map((job_title) => job_title.title)).join(', '),
        },
        {
            name: 'Email',
            selector: (row) => row.email,
        },
    ];

    const subHeaderComponentMemo = React.useMemo(() => {
		const handleClear = () => {
			if (filterText) {
				setResetPaginationToggle(!resetPaginationToggle);
				setFilterText('');
			}
		};

		return (
			<FilterComponent onFilter={e => handleFilter(e.target.value)} onClear={handleClear} filterText={filterText} />
		);
	}, [filterText, resetPaginationToggle]);

	const handlePageChange = (page) => {
        const payload = {
            page,
            perPage
        };
		fetchContacts(payload);
	};

	const handlePerRowsChange = (newPerPage, page) => {
        const payload = {
            page,
            perPage: newPerPage
        };
		fetchContacts(payload);
	};

    const handleFilter = debounce((keyword) => {

        const payload = {
            keyword,
            page: 1,
            perPage: perPage
        };
		fetchContacts(payload);
        setFilterText(keyword);
    })


    React.useEffect(() => {
        handlePageChange(1)
    }, [])

    return (
        <>
            <DataTable
                title="ContactList"
                columns={columns}
                data={contactList}
                progressPending={loading}
                pagination
                paginationServer
                paginationTotalRows={totalRows}
                onChangeRowsPerPage={handlePerRowsChange}
                onChangePage={handlePageChange}
                paginationResetDefaultPage={resetPaginationToggle}
                subHeader
                subHeaderComponent={subHeaderComponentMemo}
            />
        </>
    )
}

export default ContactList;
import React from 'react';

function MultipleSelect({ value, options, onChange, ...props }) {

  const handleOptionToggle = (option) => {
    if (value.includes(option)) 
    {
        onChange(value.filter((item) => item !== option));
    } 
    else 
    {
        onChange([...value, option]);
    }
  };

  return (
    <div className="multiple-select-container">
      <select
        multiple
        className="multiple-select"
        value={value}
        onChange={() => {}}
        {...props}
      >
        {/* Step 4: Generate options */}
        {options.map((option) => (
          <option
            key={option.value}
            value={option.value}
            onClick={() => handleOptionToggle(option.value)}
          >
            {option.label}
          </option>
        ))}
      </select>
    </div>
  );
}

export default MultipleSelect;

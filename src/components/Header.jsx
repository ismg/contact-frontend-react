import { useSelector, useDispatch } from 'react-redux'
import {Link, useNavigate} from 'react-router-dom'

import { FaSignInAlt, FaSignOutAlt } from 'react-icons/fa'

import {logout, reset} from './../features/auth/authSlice'

function Header() {
    const auth = useSelector(state => state.auth)

    const navigate = useNavigate()
    const dispatch = useDispatch()

    const onLogout = () => {
        dispatch(logout())
        dispatch(reset())
        navigate('/login')
    }
    return (
        <header className='header'>
            <div className='logo'>
                <Link to='/'>Contact App</Link>
            </div>
            <ul>
                {
                    auth?.user == null ? (
                        <>        
                            <li>
                                <Link to='/login'><FaSignInAlt />Login</Link>
                            </li>
                        </>
                    ) :(
                        <li>
                            <button className='btn' onClick={onLogout}>
                                <FaSignOutAlt />Logout
                            </button>
                        </li>
                    )
                }
            </ul>
        </header>
    )
}

export default Header
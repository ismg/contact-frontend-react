import React from 'react'
import { useSelector } from 'react-redux'
import { toast } from 'react-toastify'

import { postRequest } from "../app/requests";

import MultipleSelect from "./MultipleSelect";

const Contactform = ({ perPage, fetchContacts }) => {
	const user = useSelector((state) => state.auth.user);

	const [jobTitles, setjobTitles] = React.useState([]);
    const [jobTitleIds, setjobTitleIds] = React.useState([]);
    const [name, setName] = React.useState('');
    const [email, setEmail] = React.useState('');
    
    //Get new contact
    const getJobTitles = async() => {
        const response = await postRequest('api/job-titles/list', [])
        
        if(!response.data.error)
        {
            setjobTitles(response.data.data)
        }
        else
        {
			toast.error(response.data.message);
        }
    }

    const onSubmit = async() => {
        const payload = {
            job_title_ids: jobTitleIds,
            name: name,
            email: email
        }
    
        const response = await postRequest('api/contacts/create', payload)

        if(!response.data.error)
        {
            setjobTitles(response.data.data);

			getJobTitles();

            fetchContacts({page: 1, perPage});

			toast.success(response.data.message);
        }
        else
        {
			toast.error(response.data.message);
        }
    } 

    React.useEffect(() => {
		if(user && Object.keys(user).length !== 0)
		{
			getJobTitles();
		}
    }, [user])

  return (
    <>
        <section className="form">
            <form>
                <div className="form-group">
                    <label htmlFor="job_title_ids">Job Title</label>
                    <MultipleSelect
                        name='job_title_ids'
                        id='job_title_ids'
                        className="multiple-select"
                        value={jobTitleIds}
                        options={jobTitles?.length > 0 ? jobTitles?.map((jobTitle) => { return { label: jobTitle.title, value: jobTitle.id}; }) : []}
                        onChange={(value) => setjobTitleIds(value)}
                    />
                </div>
                <div className="form-group">
                    <label htmlFor="name">Name</label>
                    <input 
                        type='text' 
                        className='form-control' 
                        id='name' 
                        name='name' 
                        value={name}
                        onChange={(e) => setName(e.target.value)}
                        placeholder='Enter your name' 
                    />
                </div>
                <div className="form-group">
                    <label htmlFor="email">Email</label>
                    <input 
                        type='email' 
                        className='form-control' 
                        id='email' 
                        name='email' 
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                        placeholder='Enter your email' 
                    />
                </div>
                <div className="form-group">
                    <button type="button" className='btn btn-block' onClick={() => onSubmit()}>Add Contact</button>
                </div>
            </form>
        </section>
    </>
  )
}

export default Contactform
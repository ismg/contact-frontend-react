
import axios from "axios";

export const getRequest = async (url) => {
    return new Promise((resolve, reject) => {
        axios
        .get(url, {
            networkParams: { showLoader: false },
            headers: getToken(),
        })
        .then(response => resolve(response))
        .catch(err => {
            reject(err)
        });
    }); 
};

export const postRequest = async (url, data) => {
    return new Promise((resolve, reject) => {
        axios
        .post(url, data, {
            networkParams: { showLoader: false },
            headers: getToken(),
            validateStatus: status => status < 500
        })
        .then(response => {
            if(response.status == 401)
            {
                window.localStorage.removeItem('user');
            }
            resolve(response)
        })
        .catch(err => {
            reject(err)
        });
    })
};

export const getToken = () => {
    let reqHeaders = '';
    const user = JSON.parse(window.localStorage.getItem('user'));
    
    if (user?.token) 
    {
        reqHeaders = {
            Authorization: 'Bearer ' + user.token,
        }
    }

    return reqHeaders;
}
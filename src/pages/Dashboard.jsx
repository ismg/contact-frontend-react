import React from 'react'
import { useNavigate } from 'react-router-dom'
import { useSelector } from 'react-redux'

import { toast } from 'react-toastify'

import { postRequest } from "../app/requests";

import Contactform from '../components/Contactform';
import ContactList from '../components/ContactList';

function Dashboard() {
	const navigate = useNavigate()

	const user = useSelector(state => state.auth.user)
	
    const [contactList, setContactList] = React.useState([]);
	const [loading, setLoading] = React.useState(false);
	const [totalRows, setTotalRows] = React.useState(0);
	const perPage = 10;
	
    const fetchContacts = async (payload) => {
        if(user != null)
        {
            setLoading(true);
    
            const response = await postRequest('api/contacts/list', payload)
            
            if(!response.data.error)
            {
                setContactList(response.data.data)
                
                setTotalRows(response.data.recordsTotal);
            }
            else
            {
                toast.error(response.data.message);
            }
    
            setLoading(false);
        }
	};

	React.useEffect(() => {
		if(user == null)
		{
			navigate('/login');
		}
	}, [user])
	

	return (
		<section className='heading'>
			<h1>Welcome { user && user.name }</h1>
			<p>Contact Dashboard</p>
			<Contactform perPage={perPage} fetchContacts={fetchContacts} />
			<ContactList contactList={contactList} totalRows={totalRows} perPage={perPage} loading={loading} fetchContacts={fetchContacts} />
		</section>
	)
}

export default Dashboard
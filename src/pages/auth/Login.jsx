import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { useNavigate } from 'react-router-dom'

import { useFormik } from "formik";
import * as Yup from 'yup';

import { toast } from 'react-toastify'

import { FaSignInAlt } from "react-icons/fa";

import {login, reset} from './../../features/auth/authSlice'

import Spinner from "./../../components/Spinner";

const validationSchema = Yup.object().shape({
	email: Yup.string()
		.email('Entered invalid email address')
		.required('Email is required'),
	password: Yup.string()
		.required('Password is required')
		.min(8, 'Password is too short - should be 8 chars minimum.')
		.matches(/^(?=.{8,})(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=]).*$/, 'Password should contain at least one digit, at least one lower case, at least one upper case and at least 8 from the mentioned characters. i.e. ! # $ % & ? @')
})

function Login() {
	const navigate = useNavigate()
	const dispatch = useDispatch()

	const formik = useFormik({
		initialValues: {
			email: '',
			password: ''
		},
		validationSchema,
		onSubmit: values => {
			dispatch(login(values));
		},
	});

	const { user, isLoading, isSuccess, isError, message } = useSelector((state) => state.auth)


	React.useEffect(() => {
		if(isError)
		{
			toast.error(message);
		}

		if(isSuccess && user != null)
		{
			toast.success(message);
			setTimeout(() => {
				dispatch(reset())
				navigate('/')
			}, 2000);
		}

		if(!isSuccess && user != null && Object.keys(user).length > 0)
		{
			navigate('/')
		}


	}, [user, isLoading, isSuccess, isError, message, navigate, dispatch])

	if(isLoading)
	{
		return <Spinner />
	}

	return (
		<>
			<section className='heading'>
				<h1><FaSignInAlt /> Login</h1>
				<p>Login and start setting contacts</p>
			</section>
			<section className='form'>
				<form onSubmit={formik.handleSubmit}>
					<div className="form-group">
						<input 
							type='email' 
							className='form-control' 
							id='email' 
							name='email' 
							value={formik.values.email}
							onChange={formik.handleChange} 
							placeholder='Enter your email' 
						/>
					</div>
					<div className="form-group">
						<input 
							type='password' 
							className='form-control' 
							id='password' 
							name='password' 
							value={formik.values.password}
							onChange={formik.handleChange} 
							placeholder='Enter your password' 
						/>
					</div>
					<div className="form-group">
						<button type='submit' className='btn btn-block'>Submit</button>
					</div>
				</form>
			</section>
		</>
	)
}

export default Login